Shop Finder API
==============

API for mobile app Shop Finder. It includes the API itself and an `angularjs` dashboard to manage the stored data and insert new items from Google Places.

## Usage

Requires [mongoDB](https://www.mongodb.org/).

	$ git clone https://gliss@bitbucket.org/barkalastudios/shopfinder-api.git path/to/proyect/
	$ cd path/to/proyect/
	$ npm install
	$ npm start

Then point browser to `http://localhost:3000`.

## Test

Only API tests at the moment

	$ cd path/to/proyect/
	$ npm test

## Improvements

* More tests