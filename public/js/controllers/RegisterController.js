app.controller('RegisterController', [
	'$scope', 
	'usersService', 
	'AlertService',
	'AuthService',
	function($scope, usersService, AlertService, AuthService) {

	$scope.registerform = {};
	
	usersService.get()
		.success(function(data) {
			if(data.length != 0) {
				window.location = "/login";
			}
		})
		.error(function(err) {
			console.log(err);
		});
	
	$scope.register = function(registerform) {
		if(registerform.password != registerform.repeat) {
			console.log('wrong password');
		}
		registerform.admin = true;
		usersService.create(registerform)
			.success(function(data) {
				$scope.registerform = {};
				$scope.alerts = AlertService.add("success", "Admin user created successfully.")
				AuthService.login(registerform, function (res) {
					window.location = "/";
				}, function () {
					console.log('Invalid credentials.');
				});
			})
			.error(function(err) {
				console.log(err);
			});
	}
	
}]);