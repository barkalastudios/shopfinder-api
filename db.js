var mongourl = 'mongodb://mongo:27017/';

if (process.env.MONGO_HOST) {
	var mongourl = process.env.MONGO_HOST;
}

var mongoose = require('mongoose');

mongoose.set('useUnifiedTopology', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);

var Schema = mongoose.Schema;

var Shop = new Schema({
	name:  String,
	description: String,
	address: String,
	phone_number: String,
	opening_hours: Object,
	website: String,
	photos: Array,
	geolocation: {
		type: [Number],  // [<longitude>, <latitude>]
		index: '2d'      // create the geospatial index
    },
	geohash: String,
	hidden: { type: Boolean, default: 0 },
	source: String,
	source_id: {
		type: String,
		unique: true,
		dropDups: true
	},
	rating: { type: Number, default: 0 },
	price_level: { type: Number, default: 0 },
	reviews_count: { type: Number, default: 0 },
	city: String
});

var Review = new Schema({
	rating: Number,
	time: Number,
	text: String,
	shop_id: { type: String, ref: 'Shop' }
});

var City = new Schema({
	name: {
		type: String,
		unique: true,
		dropDups: true
	}
});

var AppSettings = new Schema({
	settings: Object
});

var User = new Schema({
	name: {
		type: String,
		unique: true,
		dropDups: true
	},
	password: String,
	email: {
		type: String,
		unique: true,
		dropDups: true
	},
	admin: { type: Boolean, default: false }
});

mongoose.model('Shop', Shop);
mongoose.model('Review', Review);
mongoose.model('City', City);
mongoose.model('User', User);
mongoose.model('AppSettings', AppSettings);

mongoose
	.connect(mongourl, {
		auth: { "authSource": process.env.MONGODB_DATABASE },
		dbName: process.env.MONGODB_DATABASE,
		user: process.env.MONGODB_USER,
		pass: process.env.MONGODB_PASS
	})
	.catch(error => console.error(error));
