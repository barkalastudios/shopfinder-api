const { Client } = require("@googlemaps/google-maps-services-js");
var express = require('express');
var router = express.Router();
var app = require('../app');

var mongoose = require('mongoose');
var Shop = mongoose.model('Shop');
var City = mongoose.model('City');
var Review = mongoose.model('Review');

var ngeohash = require('ngeohash');

/* Api starting point */
router.get('/', (req, res) => {
	res.json({ message: 'Welcome to the shops api' });
});

const key = "AIzaSyDDH78oK3DLTs9G1xfH798jO-5ok2FUvFQ";

router.get('/search', (req, res) => {
	const client = new Client({});

	if(!req.query.query) res.json([]);

	client
	  .textSearch({
	    params: {
	      query: req.query.query,
				pagetoken: req.query.pagetoken,
				minprice: 1,
	      key,
	    },
	    timeout: 1000
	  })
		.then(({ data }) => {
			const { results, next_page_token } = data;
			res.json({ results: results.map(serializeStore), next_page_token })
		})
	  .catch(e => res.status(400).json({ error: "There was a problem with the stores", e }));
});

const serializeStore = store => {
	const {
		geometry,
		formatted_address
	} = store;
	return {
		...store,
		address: formatted_address,
		geolocation: [geometry.location.lng, geometry.location.lat]
	};
}

router.get('/search/image', (req, res) => {
	const client = new Client({});

	if(!req.query.photoreference) res.status(400).json({ error: "No 'photoreference' provided" });

	client.placePhoto({
		params: {
			maxheight: 400,
			photoreference: req.query.photoreference,
			key,
		},
		timeout: 1000
	})
	.then(({ request }) => res.json(request.res.responseUrl))
	.catch(e => res.status(404).json({ error: "Photo not found", e }));
});

/* Get a list of all shops. If latitude and longitude
 * are given, it brings only the nearest shops.
 */
router.get('/shops', (req, res) => {

	var latitude = req.query.latitude;
	var longitude = req.query.longitude;
	var precision = req.query.precision || 3;
	var maxDistance = req.query.maxDistance || 15;

	if(latitude && longitude && maxDistance != -1) {
		var retShops = findShopsByLocation([longitude, latitude], maxDistance, function(shops) {
			if(shops.length == 0) {
				Shop.find({}, function (err, data) {
					if (err) return console.error(err);
					shops = shops.map(cleanOpeningHours);
					shops = shops.map(setOldLocationObject);
					res.json(shops);
				})
				.limit(50);
			} else {
				shops = shops.map(cleanOpeningHours);
				var retShops = shops.map(setOldLocationObject);
				res.json(retShops);
			}
		});
	} else {
		Shop.find({})
			.then(data => {
				shops = data.map(cleanOpeningHours);
				shops = data.map(setOldLocationObject);
				res.json(shops);
			})
			.catch(error => console.error(error));
	}
});

function cleanOpeningHours(shop) {
	shop.opening_hours = shop.opening_hours && shop.opening_hours.weekday_text ? shop.opening_hours.weekday_text : {};
	return shop;
}

function setOldLocationObject(shop) {
	shop.location = new Object();
	shop.location = {
		lat: shop.geolocation[0],
		lng: shop.geolocation[1],
	};
	return shop;
}

/* Save a new shop, and its reviews */
router.post('/shops/new', (req, res, next) => {
 	const place_id = req.body.place_id;
	if(!place_id) res.status(400).json({ error: "place_id must not be empty" });

	const client = new Client({});

	client.placeDetails({
		params: {
			place_id,
			key
		},
		timeout: 1000
	})
	.then(({ data }) => {
		const newStore = new Shop(serializeStoreDetail(data.result));
		newStore.save()
			.then(data => {
				var newCity = new City({
					name: data.city
				});
				newCity.save();
				res.json(data)
			})
			.catch(error =>
				res
					.status(500)
					.json({ message: "There was an error saving the new store", error })
				)
	})
	.catch(e => res.status(404).json({ error: "Place not found", e }));
})

const serializeStoreDetail = store => {
	const {
		name,
		opening_hours,
		website,
		rating,
		price_level,
		reviews_count,
		geometry,
		formatted_address,
		address_components = [],
		international_phone_number,
		place_id,
		photos = [],
		reviews = []
	} = store;
	console.log(store);
	return {
		name,
		opening_hours,
		website,
		rating,
		price_level,
		reviews_count: reviews.length,
		address: formatted_address,
		phone_number: international_phone_number,
		source_id: place_id,
		city: address_components.find(({ types }) => types.includes('locality')).long_name,
		geohash: encodeCoords(geometry.location),
		photos: retrieveNewPhotos(photos),
		geolocation: [geometry.location.lng, geometry.location.lat]
	};
}

/* Save a new shop, and its reviews */
router.post('/shops', function(req, res, next) {

	var newShop = new Shop();
	saveNewShop(req.body, newShop);

	newShop.save(function(err, shop) {
		if (err) { return next(err); }

		/* We save the shop city if still not in the database */
		var newCity = new City({
			name: shop.city
		});

		newCity.save(function(err, city) {
			if (err) console.log("error: " + err);
		});

		/* If the shop brings reviews, we save each */
		retrieveNewReviews(newShop, req.body);

		res.json(shop);
	});
});

/*
 * Saves a new shop given remote info
 */
function saveNewShop(shop_info, newShop) {

	var reviews = shop_info.reviews;
	var geohash = encodeCoords(shop_info.geometry.location);

	newShop.name = shop_info.name;
	newShop.description = shop_info.description;
	newShop.address = shop_info.formatted_address;
	newShop.phone_number = shop_info.international_phone_number;
	newShop.opening_hours = shop_info.opening_hours;
	newShop.website = shop_info.website;
	newShop.city = shop_info.city;
	newShop.geolocation = [shop_info.geometry.location.lng, shop_info.geometry.location.lat];
	newShop.geohash = geohash;
	newShop.hidden = shop_info.permanently_closed;
	newShop.source = shop_info.source;
	newShop.source_id = shop_info.place_id ? shop_info.place_id : shop_info.source_id;
	newShop.rating = shop_info.rating ? shop_info.rating : 0;
	newShop.price_level = shop_info.price_level ? shop_info.price_level : 0;
	newShop.reviews_count = reviews ? reviews.length : 0;

	if(shop_info.photos && shop_info.photos.length > 0)
		newShop.photos = shop_info.photos[0].photo_reference ? retrieveNewPhotos(shop_info.photos) : shop_info.photos;
	else
		newShop.photos = [];
}

/* Get a shop detail by id, but not its reviews  */
router.get('/shops/:shop_id', function(req, res, next) {
	Shop.findById(
		req.params.shop_id,
		function (err, data) {
			if (err) { return next(err); }
			res.json(data);
		});
});

/* Updates a shop details */
router.put('/shops/:shop_id', function(req, res, next) {
	Shop.findByIdAndUpdate(
		req.params.shop_id,
		{
			description: req.body.description
		},
		function (err, data) {
			if (err) {
				console.error(err);
				return next(err);
			}
			res.json(data);
	});
});

/* Get all reviews  */
router.get('/reviews', function(req, res, next) {
	Review.find({
	}, function (err, data) {
		if (err) { return next(err); }
		res.json(data);
	});
});

/* Get a list of a shop's reviews */
router.get('/shops/:shop_id/reviews', function(req, res, next) {
	Review.find({
		shop_id: req.params.shop_id
	}, function (err, data) {
		if (err) { return next(err); }
		res.json(data);
	});
});

/* Check if a shop exists given its source id
 * (ie, Google Places ID)
 */
router.get('/shops/exists/:source_id', function(req, res, next) {
	Shop.find({ source_id: req.params.source_id })
		.then(data => res.json(data.length > 0))
		.catch(err => next(err));
});

/* Remove a stored shop given its id */
router.delete('/shops/:shop_id', function(req, res, next) {
	if(req.params.shop_id === undefined) return console.error("No _id provided" );

	Shop.remove({
		_id: req.params.shop_id
	}, function (err, shopData) {
		if (err) return next(err);

		/* We remove all its reviews as well */
		Review.remove({
			shop_id: req.params.shop_id
		}, function (err, data) {
			if (err) return next(err);
			res.json(shopData);
		});
	});
});

/* Empty the DB, removes all shops, reviews and cities */
router.get('/removeall', function(req, res) {
	City.remove({}, function(err) {
		Shop.remove({}, function(err) {
			Review.remove({}, function(err) {
				res.json({ message: 'All data removed' });
			});
		});
	});
});

/* Run schema updates */
router.get('/schema_updates', function(req, res) {
	Shop.find({}, function (err, shops) {
		if (err) return console.error(err);
		shops.forEach(function(shop, index) {
			if(shop.geolocation.geohash)
				shop.geohash = shop.geolocation.geohash;
			if(shop.geolocation.location)
				shop.geolocation = [shop.geolocation.location.lng, shop.geolocation.location.lat]
			shop.save();
		});
		res.json(shops.length);
	});
});

/* Get a list of the available cities */
router.get('/city', function(req, res, next) {
	City.find({}, function (err, data) {
		if (err) return next(err);
		res.json(data);
	});
});

/* Update data retrieved from google places */
router.get('/updateData', function(req, res, next) {
	Shop.find({}, function (err, data) {
		if (err) return next(err);

		data.forEach(function(shop) {
			locations.details({placeid: shop.source_id},
				function(err, details) {

				if(err) {
					console.log(err);
					return;
				}

				if(!details.result) {
					console.error("Error: ");
					console.error(details);
				} else {
					saveNewShop(details.result, shop);

					// We save new reviews
					retrieveNewReviews(shop, details.result);

					// We bring the new number of reviews
					var new_reviews_count = 0;

					Review.find({
						shop_id: shop._id
					}, function (err, data) {
						if (err) return console.error(err);
						new_reviews_count = data.length;
						shop.reviews_count = new_reviews_count;
						shop.save();
					});

				}
			});
		});

		res.json({ message: 'All data updated' });
	});
});

/*
 * Retrieves all reviews given the shop, and saves only the new ones.
 */
function retrieveNewReviews(shop, remote_shop) {
	var currShopReviews = [];

	if (remote_shop.reviews) {

		Review.find({
			shop_id: shop._id
		}, function (err, data) {
			if (err) return console.error(err);
			currShopReviews = data;

			for (var i = 0; i < remote_shop.reviews.length; i++) {
				if (!reviewAlreadySaved(currShopReviews, remote_shop.reviews[i])) {

					var newReview = new Review({
						shop_id: shop._id,
						rating: remote_shop.reviews[i].rating,
						text: remote_shop.reviews[i].text,
						time: remote_shop.reviews[i].time
					});

					newReview.save(function(err, review) {
						if (err) {
							console.log("error:" + err);
						}
					});

				};
			};
		});

	};

}

/*
 * Checks if a given remote review is already
 * persisted as part of a shop, or not.
 */
function reviewAlreadySaved(reviews, new_review) {
	for (var i = 0; i < reviews.length; i++) {
		if(reviews[i].time == new_review.time) {
			return true;
		}
	};
	return false;
}

/*
 * Given a remote array of photos, it retrieves the url
 * for each and returns it in an array.
 */
function retrieveNewPhotos(remote_photos) {
	shopPhotosUrls = [];
	if (remote_photos) {
		for (var i = 0; i < remote_photos.length; i++) {
			locations.photo({
				photoreference: remote_photos[i].photo_reference,
				maxwidth: 400
			}, function(err, photo) {
				if (err) console.log(err);
				shopPhotosUrls.push(photo);
			});
		};
	};

	return shopPhotosUrls;
}

/**
 * Finds the city of a given google place
 */
function getLocality(shop) {
	// TODO: this variable should be persisted and updated from the UI
	var CITY_COMPONENT = 'locality';
	var shopCity = '';

	if(shop.address_components) {
		shop.address_components.forEach(function(component) {
			component.types.forEach(function(componentType) {
				if(componentType == CITY_COMPONENT) {
					shopCity = component.long_name;
					return false;
				}
			});
			if(shopCity != '') return false;
		});
		return shopCity;
	}
}

/**
 * Given a coords array [longitude, latitude] and a maxDistance,
 * finds all shopws for the coordante within that distance.
 */
function findShopsByLocation(coords, maxDistance, callback) {
	Shop.find( {
		geolocation: {
			$near: coords,
			$maxDistance: maxDistance/111.12
		}
	}, function( err, shops, count ) {
		if(err) console.log(err);
		callback(shops);
	});
}

/**
 * Given a hashstring and a precision, finds all shops
 * for the coordante represented by the hashstring and
 * all its neighbors.
 */
function findShopsWithGeohash(hashstring, precision, callback) {

	// Sets the precision of the hashstring.
	var wrapSquare = hashstring.substring(0, precision);
	var centerSquare = hashstring.substring(0, precision);

	console.log(wrapSquare);

	// Get the 8 neighbors of that box, along with the original box
	// let neighborsHashstrings = geohash.neighbors(wrapSquare);
	// neighborsHashstrings.push(wrapSquare);

	Shop.find( { geohash: { $regex: wrapSquare + '.*'} }, function( err, shops, count ) {
		if(err) console.log(err);
		callback(shops);
	});
}

/**
 * Encodes coordantes. Parameter should be a json
 * with lat and lng parameters.
 */
function encodeCoords(location) {
	return ngeohash.encode(location['lat'], location['lng']);
}

/* TODO: Remove all from here */
var GoogleLocations = require('google-locations');
var locations = new GoogleLocations('AIzaSyDDH78oK3DLTs9G1xfH798jO-5ok2FUvFQ');

module.exports = router;
