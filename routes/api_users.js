var express = require('express');
var router = express.Router();
var app = require('../app');

var mongoose = require('mongoose');
var User = mongoose.model('User');
var passwordHash = require('password-hash');

/* If users table is empty, create default user admin:admin */
const createAdminUser = () => {
	User.find({})
		.then(users => {
			if (users.length == 0) {
				var new_user = new User({
					name: 'admin',
					password: passwordHash.generate('admin'),
					email: 'admin@admin.com',
					admin: true
				});

				new_user.save()
					.then()
					.catch(err => next(err));
			}
		})
		.catch(error => console.error(error));
}

/* We create admin user if the users table is empty */
createAdminUser();

/**
 * Saves a new user.
 */
router.post('/', (req, res, next) => {
	var new_user = new User({
		name: req.body.name,
		password: passwordHash.generate(req.body.password),
		email: req.body.email,
		admin: req.body.admin
	});

	new_user.save()
		.then(data => res.json(data))
		.catch(err => next(err));
});

const serializeUser = ({ name, admin, email, _id }) => ({ name, admin, email, _id });

/* Get all users */
router.get('/', (req, res, next) =>
	User.find({})
			.then(users => res.json(users.map(user => serializeUser(user))))
			.catch(err => next(err))
);

/* Get details of specified user */
router.get('/:user_id', (req, res, next) =>
	User.findById(req.params.user_id)
			.then(data => res.json(serializeUser(data)))
			.catch(err => next(err))
);

/* Update details of a specified user */
router.put('/:user_id', function(req, res, next) {
	User.findByIdAndUpdate(
		req.params.user_id,
		{
			name: req.body.name,
			password: passwordHash.generate(req.body.password),
			email: req.body.email,
			admin: req.body.admin
		},
		function (err, data) {
			if (err) return next(err);
			res.json(data);
	});
});

/* Delete specified user */
router.delete('/:user_id', (req, res, next) => {
	User.remove({ _id: req.params.user_id })
			.then(data => res.json(data))
			.catch(err => next(err))
});

module.exports = router;
