'use strict';

var express = require('express');
var router = express.Router();
var app = require('../app');

var mongoose = require('mongoose');
var AppSettings = mongoose.model('AppSettings');

const defaultData = {
  "title": "AppSettings",
  "menu": {
    "textColor": "#FFFFFF",
    "sections": [
      {
        "name": "",
        "rows": [
          {
            "action": {
              "type": "link",
              "data": {
                "str_link": "http://barkalastudios.com"
              }
            },
            "title": {
              "es": "Inicio",
              "en": "Home",
              "fr": "Accueil"
            },
            "icon": {
              "class": 0,
              "type": 513,
              "color": "#FFFFFF"
            }
          },
          {
            "action": {
              "type": "link",
              "data": {
                "str_link": "http://barkalastudios.com"
              }
            },
            "title": {
              "es": "Cercanía",
              "en": "Nearby"
            },
            "icon": {
              "class": 0,
              "type": 166,
              "color": "#FFFFFF"
            }
          },
          {
            "title": {
              "en": "About",
              "es": "Acerca"
            },
            "icon": {
              "class": 0,
              "type": 178,
              "color": "#FFFFFF"
            },
            "action": {
              "type": "link",
              "data": {
                "str_link": "http://barkalastudios.com"
              }
            }
          },
          {
            "title": {
              "es": "Preguntas Frecuentes",
              "en": "FAQ"
            },
            "icon": {
              "class": 0,
              "type": 769,
              "color": "#FFFFFF"
            },
            "action": {
              "type": "link",
              "data": {
                "str_link": "http://barkalastudios.com"
              }
            }
          },
          {
            "action": {
              "type": "link",
              "data": {
                "str_link": "http://barkalastudios.com"
              }
            },
            "title": {
              "es": "Opciones",
              "en": "Settings"
            },
             "icon": {
              "class": 0,
              "type": 264,
              "color": "#FFFFFF"
            }
          }
        ]
      }
    ]
  },
  "shopDetail": {
    "localization": {
      "str_openingHours": {
        "en": "Opening Hours",
        "es": "Horas de apertura",
        "fr": ""
      },
      "str_gallery": {
        "en": "Gallery",
        "es": "Galeria de imágenes",
        "fr": ""
      },
      "str_reviews": {
        "en": "Reviews",
        "es": "Comentarios",
        "fr": ""
      },
      "str_map": {
        "en": "Map",
        "es": "Mapa",
        "fr": ""
      }
    },
    "style": {
      "tableSectionColor": "#FEFEFE",
      "tableIconsColor": "#FEFEFE"
    }
  },
  "config": {
    "aboutLink": "http://gettappas.com/",
    "faqLink": "http://gettappas.com/faq",
    "privacyLink": "http://gettappas.com/privacy-policy",
    "appMenuBackgroundImageLink": "http://www.barkalastudios.com/wp-content/uploads/2016/04/back.jpg",
    "shopSelectionCountToShowAd": 100,
    "gallerySelectionCountToShowAd": 100,
    "menuSelectionCountToShowAd": 100
  },
  "nearby": {
    "mapAnnotationImageLink": "http://icons.iconarchive.com/icons/paomedia/small-n-flat/64/map-marker-icon.png"
  },
  "inAppPurchase": {
    "gradients": [
      "#bfff",
      "#cfff",
      "#dfff",
      "#cfff",
      "#afff",
      "#fcff"
    ],
    "bannerFullLink": "http://www.barkalastudios.com/wp-content/uploads/2016/04/back.jpg",
    "purchaseButtonGlowColor": "#FFFF3",
    "gradientAnimationDuration": 4
  },
  "sortFilter": {
    "localization": {
      "str_sort_distance": {
        "es": "Distancia",
        "en": "Distance"
      },
      "str_sort_rating": {
        "es": "Puntaje",
        "en": "Rating"
      },
      "str_sort_review": {
        "es": "Review",
        "en": "Review"
      },
      "str_sort_title": {
        "es": "Filtro",
        "en": "Filter"
      }
    }
  },
  "style": {
    "navBarTintColor": "#f7947d",
    "navBarButtonItemTintColor": "#ffffff",
    "navBarTitleColor": "#ffffff",
    "navBarTitleSize": 30,
    "primaryColor": "f7947d",
    "secondaryColor": "#ffffff"
  },
  "share": {
    "appShareImageLink": "http://www.picturescolourlibrary.co.uk/loreswithlogo/1917011.jpg",
    "appShareMessage": {
      "en": "Check Tappas! Best app to find near tapas restaurants!",
      "es": "Tappas, la aplicación mas copada del universo. Comparte con amigos y perros!"
    },
    "appShareLink": "http://barkalastudios.com"
  },
  "settings": {
    "backgroundColor": "#FFFFFF",
    "sections": [
      {
        "name": "SUPPORT",
        "rows": [
          {
            "title": "Help and Feedback",
            "icon": {
              "class": 1,
              "type": 11,
              "color": "#FFCC00"
            },
            "action": {
              "type": "link",
              "data": "http://barkalastudios.com"
            }
          },
          {
            "title": "Email Us",
            "icon": {
              "class": 1,
              "type": 13,
              "color": "#006699"
            },
            "action": {
              "type": "dlink",
              "data": {
                "dlink": "mailto:hello@gettappas.com?subject=Greetings%20from%20Cupertino!&body=Wish%20you%20were%20here!"
              }
            }
          },
          {
            "title": "Add Features",
            "icon": {
              "class": 1,
              "type": 3,
              "color": "#2CA390"
            },
            "action": {
              "type": "mail",
              "data": {
                "to": "hello@gettappas.com",
                "subject": "Email Us",
                "body": "html body"
              }
            }
          }
        ]
      },
      {
        "name": "SOCIAL",
        "rows": [
          {
            "title": "Rate this App",
            "icon": {
              "class": 1,
              "type": 4,
              "color": "#E60073"
            },
            "action": {
              "type": "dlink",
              "data": {
                "dlink": "http://itunes.apple.com/app/id378458261",
                "link": "http://itunes.apple.com/app/id378458261",
                "appName": "App Store"
              }
            }
          },
          {
            "title": "Follow Us on Twitter",
            "icon": {
              "class": 1,
              "type": 150,
              "color": "#4099FF"
            },
            "action": {
              "type": "dlink",
              "data": {
                "dlink": "twitter:///user?screen_name=spiritsciences",
                "link": "https://twitter.com/spiritsciences",
                "appName": "Twitter"
              }
            }
          },
          {
            "title": "Like Us on Facebook",
            "icon": {
              "class": 1,
              "type": 100,
              "color": "#3B5998"
            },
            "action": {
              "type": "dlink",
              "data": {
                "dlink": "fb://profile/113810631976867",
                "link": "https://www.facebook.com/thespiritscience",
                "appName": "Facebook"
              }
            }
          },
          {
            "title": "Terms of Service",
            "icon": {
              "class": 1,
              "type": 197,
              "color": "#006600"
            },
            "action": {
              "type": "link",
              "data": "http://barkalastudios.com"
            }
          },
          {
            "title": "Privacy Policy",
            "icon": {
              "class": 1,
              "type": 5,
              "color": "#cc3300"
            },
            "action": {
              "type": "link",
              "data": "http://barkalastudios.com"
            }
          }
        ]
      }
    ]
  }
}

/* If settings table is empty, create default settings data */
const createDefaultSettings = () => {
	AppSettings.findOne({})
		.then(data => {
			if (!data) {
				const newSettings = new AppSettings({
					settings: { ...defaultData, updated: Date.now() }
				});
				newSettings
					.save()
					.catch(error => next(error));
			};
		})
		.catch(error => console.error(error));
}

createDefaultSettings();

/* Get settings for the client app given a lang parameter */
router.get('/', (req, res) => {
	AppSettings.findOne({})
		.then(data => {
			if (!data) res.json({});

			const { query: { lang = 'en' } } = req;

			var result = [];
			customFilter(data.settings, result, lang);

			res.json(data.settings);
		})
		.catch(error => next(err));
});

/* Get full settings for the dashboard */
router.get('/full', (req, res) => {
	AppSettings.findOne({})
		.then(data => res.json(data ? data.settings : {}))
		.catch(error => next(error));
});

function customFilter(object, result, lang) {
	['title', 'appShareMessage'].forEach(function(prop, i, a) {
		if(object.hasOwnProperty(prop)) {
			object[prop] = filterLangObject(object[prop], lang);
			result.push(object[prop]);
		}
	});

	var keyRegex = /^str_/;

	for(var key in object) {
		if(typeof object[key] == "object") {
			if (key.match(keyRegex)) {
				object[key] = filterLangObject(object[key], lang);
				result.push(object[key]);
			} else {
				customFilter(object[key], result, lang);
			}
		}
	}
}

function filterLangObject(stringObject, lang) {
	var default_lang = 'en';
	if(typeof stringObject == "object") {
		if(stringObject.hasOwnProperty(lang)) {
			stringObject = stringObject[lang];
		} else {
			stringObject = stringObject[default_lang];
		}
	}
	return stringObject;
}

/* Save settings */
router.post('/', (req, res, next) => {
	AppSettings.findOne({})
	.then(data => {
		if(data != null) {
			data.settings = req.body;
			data.settings.updated = Date.now();
			data.save().then(data => res.json(data));
		} else {
			var newSettings = new AppSettings({
				settings: req.body
			});
			newSettings.settings.updated = Date.now();
			newSettings.save().then(data => res.json(data));
		}
	})
	.catch(error => next(error));
});

module.exports = router;
